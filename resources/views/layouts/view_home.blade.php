
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.themefisher.com/focus/app-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:11:07 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Focus - Bootstrap Admin Dashboard </title>
    <!-- Favicon icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('admin/images/favicon.png') }} ">
    <link href="{{ asset('admin/css/style.css')}}" rel="stylesheet">
    @yield('css')
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="" class="brand-logo">
                <img class="logo-abbr" src="{{ asset('admin/images/logo.png') }} " alt="">
                <img class="logo-compact" src="{{ asset('admin/images/logo-text.png') }}  " alt="">
                <img class="brand-title" src="{{ asset('admin/ images/logo-text.png') }}" alt="">
            </a>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="search_bar dropdown">
                                <span class="search_icon p-3 c-pointer" data-toggle="dropdown">
                                    <i class="mdi mdi-magnify"></i>
                                </span>
                                <div class="dropdown-menu p-0 m-0">
                                    <form>
                                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <ul class="navbar-nav header-right">
                           
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                    <i class="mdi mdi-account"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="" class="dropdown-item">
                                        <i class="icon-user"></i>
                                        <span class="ml-2">{{auth()->user()->username}} </span>
                                    </a>
                                    <a href="{{ asset('email-inbox.html') }} " class="dropdown-item">
                                        <i class="icon-envelope-open"></i>
                                        <span class="ml-2">Inbox </span>
                                    </a>
                                    <a href="{{route('logout')}}" class="dropdown-item">
                                        <i class="icon-key"></i>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label first">Main Menu</li>
                    <!-- <li><a href="index.html"><i class="icon icon-single-04"></i><span class="nav-text">Dashboard</span></a>
                    </li> --> 
                    @if (auth()->user()->role==0)
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-single-04"></i><span class="nav-text">Admin</span></a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('users.index') }} ">User</a></li>
                            </ul>
                    </li>
                   
                        
                    <li><a href="{{route('categories.index')}}"> <i class="incon iconfa-spin"></i><span class="nav-text">Category</span></a></li>
                    @elseif(auth()->user()->role==1)
                  <li><a href="{{route('products.index')}}" aria-expanded="false"> <i class="incon iconfa-spin"></i><span class="nav-text">Product</span></a>
                  <li><a href="{{route('order.index')}}" aria-expanded="false"> <i class="incon iconfa-spin"></i><span class="nav-text">Order</span></a>
                    @endif
                </ul>
            </div>


        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
   
        @yield('content')
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Quixkit</a> 2019</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->

        
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    @yield('js')
    <script src="{{ asset('admin/vendor/global/global.min.js') }} "></script>
    <script src="{{ asset('admin/js/quixnav-init.js') }} "></script>
    <script src="{{ asset('admin/js/custom.min.js') }} "></script>
    

</body>


<!-- Mirrored from demo.themefisher.com/focus/app-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:12:15 GMT -->
</html>