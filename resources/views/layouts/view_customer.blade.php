<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="_token" content="{{ csrf_token() }}">
    <title>SuperMart - eCommerce HTML5 Template</title>
    <meta name="description"
        content="SuperMart eCommerce HTML Template specially designed for multipurpose shops like the mega store, grocery stores, supermarkets, organic shops, and online stores" />
    <link rel="shortcut icon" href="https://demo.themeies.com/supermart-html/assets/images/logo/logo.png"
        type="image/f-icon" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('customer/css/all.min.css') }}" />
    <!-- bootstraph -->
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }} " />
    <!-- Fancy Box -->
    <link rel="stylesheet" href="{{ asset('customer/css/jquery.fancybox.min.css') }} " />
    <!-- swiper js -->
    <link rel="stylesheet" href="{{ asset('customer/css/swiper-bundle.min.css') }}" />
    <!-- Nice Select -->
    <!-- Countdown js -->
    <link rel="stylesheet" href="{{ asset('customer/css/jquery.countdown.css') }}" />
    <!-- User's CSS Here -->
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }} " />
</head>

<body>
    <div id="notice"></div>
    <!-- Header Start  -->
    <header class="header header-sticky">
        <div class="container">
            <!-- Header Top Start -->
            <div class="header__top">
                <div class="header__left">
                    <!-- Header Toggle Start -->

                    <!-- Header Toggle End -->
                    <div class="header__logo">
                        <a href="{{route('customer')}}"><img src="{{ asset('customer/images/logo.png') }} " alt="logo" /> </a>
                    </div>
                    <div class="search__form__wrapper">
                        <form action="" method="get" class="search__form">

                            <input type="text" class="form-control" name="key"
                                placeholder="What are you looking for..." />
                            <button type="submit">
                                <img src="{{ asset('customer/images/search.png') }} " alt="search" />
                            </button>
                        </form>
                    </div>
                </div>
                <div class="header__meta">

                    <div class="header__cart language__select custom__dropdown">

                        <div class="miniCart">
                            <div class="header__cart">
                                <a href="{{ route('history.index') }}" class="cartbtn">
                                    <span class="title">History</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="miniCart">
                        <div class="header__cart">
                            <a href="{{ route('cart.index') }}" class="cartbtn">
                                <div class="cart__btn-img">
                                    <img src=" {{ asset('customer/images/cart-icon.png') }} " alt="cart-icon" />
                                    <span class="value">10</span>
                                </div>
                                <span class="title">cart</span>
                            </a>
                        </div>
                    </div>
                        <ul class="meta__item">
                            <li>
                                @if (isset(auth()->user()->id))
                                   <a href="{{ route('logout') }}">
                                        <i class="fa-solid fa-user-plus"></i>
                                        <span>Logout</span>
                                   </a>
                                @else
                                <a href="{{ route('login') }}">
                                    <i class="fa-solid fa-user-plus"></i>
                                    <span>Sign up</span>
                                </a>
                                @endif

                            </li>
                        </ul>
                        
                    </div>
                    <!-- Header Top End -->
                </div>
                <!-- Search Form -->
                <form action="search.html" method="post" class="search__form full__width d-lg-none d-flex">
                    <input type="search" class="form-control" name="search"
                        placeholder="What are you looking for..." />
                    <button type="submit">
                        <img src="{{ asset('customer/images/search.png') }} " alt="search" />
                    </button>
                </form>
                <!-- Search Form -->
            </div>

    </header>


    @yield('content')





    </section>
    <!-- ........................footer........................ -->
    <section class="call__to__action">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta__box__wrapper text-center">
                        <h2 class="cta__title">Get notified when we're launching new products</h2>
                        <p class="cta__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet leo, porta
                            aliquet non mattis. Nulla ante pellentesque blandit fermentum.</p>
                        <form action="#" method="post" class="cta__form">
                            <input type="email" name="email" class="form-control" placeholder="Enter your email" />
                            <button type="submit" class="btn btn-primary">Notify me</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- CAll To Action End -->

    <!-- Feature Section Start -->

    <!-- Feature Section End -->

    <!-- Footer Section Start -->
    <footer class="footer__section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="footer__wrapper">
                        <div class="footer__widget d-block">
                            <div class="footer__logo">
                                <a href="home.html"><img src=" {{ asset('customer/images/logo.png') }}"
                                        alt="logo" /></a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. At praesent eget mauris felis
                                scelerisque enim enim, magna.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <div class="footer__wrapper">
                        <div class="footer__widget">
                            <div class="title">
                                <h5>Product</h5>
                            </div>
                            <ul>
                                <li><a href="#">Shirt</a></li>
                                <li>
                                    <a href="#">Groceries <span class="danger">Hot</span></a>
                                </li>
                                <li>
                                    <a href="#">Computer <span>New</span></a>
                                </li>
                                <li><a href="#">Electronics</a></li>
                                <li><a href="#">Sports</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <div class="footer__wrapper">
                        <div class="footer__widget">
                            <div class="title">
                                <h5>Company</h5>
                            </div>
                            <ul>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#">Media kit</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <div class="footer__wrapper">
                        <div class="footer__widget">
                            <div class="title">
                                <h5>Resources</h5>
                            </div>
                            <ul>
                                <li><a href="#">Blog</a></li>
                                <li>
                                    <a href="#">Newsletter <span>Best</span></a>
                                </li>
                                <li><a href="#">Help centre</a></li>
                                <li><a href="#">Tutorials</a></li>
                                <li><a href="#">Support</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <div class="footer__wrapper">
                        <div class="footer__widget">
                            <div class="title">
                                <h5>Legal</h5>
                            </div>
                            <ul>
                                <li><a href="#">Terms</a></li>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Licenses</a></li>
                                <li><a href="#">Settings</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Accordion Start -->
                    <div class="accordion__wrapper">
                        <div class="accordion accordion-flush" id="accordionFlushExample01">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading01">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#flush-collapse01"
                                        aria-expanded="false" aria-controls="flush-collapse01">
                                        Product
                                    </button>
                                </h2>
                                <div id="flush-collapse01" class="accordion-collapse collapse"
                                    aria-labelledby="flush-heading01" data-bs-parent="#accordionFlushExample01">
                                    <div class="accordion-body">
                                        <div class="footer__widget">
                                            <ul>
                                                <li><a href="#">Shirt</a></li>
                                                <li>
                                                    <a href="#">Groceries <span class="danger">Hot</span></a>
                                                </li>
                                                <li>
                                                    <a href="#">Computer <span>New</span></a>
                                                </li>
                                                <li><a href="#">Electronics</a></li>
                                                <li><a href="#">Sports</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading02">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#flush-collapse02"
                                        aria-expanded="false" aria-controls="flush-collapse02">
                                        Company
                                    </button>
                                </h2>
                                <div id="flush-collapse02" class="accordion-collapse collapse"
                                    aria-labelledby="flush-heading02" data-bs-parent="#accordionFlushExample01">
                                    <div class="accordion-body">
                                        <div class="footer__widget">
                                            <ul>
                                                <li><a href="#">About us</a></li>
                                                <li><a href="#">Careers</a></li>
                                                <li><a href="#">News</a></li>
                                                <li><a href="#">Media kit</a></li>
                                                <li><a href="#">Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading03">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#flush-collapse03"
                                        aria-expanded="false" aria-controls="flush-collapse03">
                                        Resources
                                    </button>
                                </h2>
                                <div id="flush-collapse03" class="accordion-collapse collapse"
                                    aria-labelledby="flush-heading03" data-bs-parent="#accordionFlushExample01">
                                    <div class="accordion-body">
                                        <div class="footer__widget">
                                            <ul>
                                                <li><a href="#">Blog</a></li>
                                                <li>
                                                    <a href="#">Newsletter <span>Best</span></a>
                                                </li>
                                                <li><a href="#">Help centre</a></li>
                                                <li><a href="#">Tutorials</a></li>
                                                <li><a href="#">Support</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading04">
                                    <button class="accordion-button collapsed" type="button"
                                        data-bs-toggle="collapse" data-bs-target="#flush-collapse04"
                                        aria-expanded="false" aria-controls="flush-collapse04">
                                        Legal
                                    </button>
                                </h2>
                                <div id="flush-collapse04" class="accordion-collapse collapse"
                                    aria-labelledby="flush-heading04" data-bs-parent="#accordionFlushExample01">
                                    <div class="accordion-body">
                                        <div class="footer__widget">
                                            <ul>
                                                <li><a href="#">Terms</a></li>
                                                <li><a href="#">Privacy</a></li>
                                                <li><a href="#">Licenses</a></li>
                                                <li><a href="#">Settings</a></li>
                                                <li><a href="#">Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion End -->
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="footer__content text-center">
                            <div class="content">
                                <p>
                                    &copy; 2022
                                    <a href="#">SuperMart</a>. All rights reserved
                                </p>
                            </div>
                            <div class="link">
                                <a href="#"><i class="fa-brands fa-twitter"></i> </a>
                                <a href="#"><i class="fa-brands fa-linkedin"></i></a>
                                <a href="#"><i class="fa-brands fa-facebook"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Header Flyout Menu Start -->
    <div class="flyoutMenu">
        <div class="flyout__flip">
            <div class="flyout__inner">
                <div class="menu__header-top">
                    <div class="brand__logo">
                        <a href="home.html"><img src="{{ asset('customer/images/logo.png') }}" alt="logo" /></a>
                    </div>
                    <!-- Close -->

                </div>
                <!-- Search Form -->
                <form action="search.html" method="post" class="search__form full__width">
                    <input type="search" class="form-control" name="search"
                        placeholder="What are you looking for..." />
                    <button type="submit">
                        <img src="{{ asset('customer/images/search.png') }} " alt="search" />
                    </button>
                </form>
                <!-- Search Form -->

            </div>
        </div>
    </div>


    </div>
    <!-- Product Preview Modal End -->

    <!-- Preloader Start -->
    <div id="preloader">
        <div id="status"><img src="{{ asset('customer/images/favicon.png') }} " alt="logo" /></div>
    </div>
    <!-- Preloader End -->

    <!-- Scroll-top -->
    <button class="scroll-top scroll-to-target" data-target="html">scroll</button>
    <!-- Scroll-top-end-->

    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css"
        integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
    
    @yield('js')
    <script src="{{ asset('customer/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('customer/js/popper.min.js') }}"></script>
    <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('customer/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('customer/js/jquery.plugin.min.js') }}"></script>
    <script src="{{ asset('customer/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('customer/js/counterup.min.js') }}"></script>
    <script src="{{ asset('customer/js/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('customer/js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('customer/js/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('customer/js/scripts.js') }}"></script>
</body>

</html>
