@extends('layouts.view_home')
@section('content')
<div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Restore Category</h3>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="p-2 bd-highlight"><a href="{{ route('categories.create') }}" class="btn btn-success">Add</a></div>
                {{-- <div class="p-2 bd-highlight">
                    <form action="" method="GET">
                        @csrf
                        <button class="btn btn-info" style="text-decoration: none; color: #ffff;" type="submit"
                            name="export_exel">Export excel</button>
                    </form>
                </div> --}}
                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th>STT</th>
                            <th>Tên loại sản phẩm</th>
                            <th>Image</th>
                            <th style="width:20px;">Restore</th>
                            <th style="width:20px;">Delete</th>
                        </tr>
                    </thead>
                    @foreach ($softCategories as $softCategory)
                        <tr>
                        <td>{{$i++}}</td>
                        <td>{{$softCategory->name}}</td>
                        <td><img style="height: 50px;width: 50px; " src="/uploads/{{$softCategory->image}}"></td>
                        <td style="width:20px;"><a  class="btn btn-primary" href="{{route('category.restore',$softCategory->id)}}">Restore</a></td>
                        <td style="width:20px;"><form action="{{route('category.realDelete',$softCategory->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                            </form></td>
                    </tr>
                    @endforeach
                    <tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
