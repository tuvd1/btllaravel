@extends('layouts.view_home')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.css"
        integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
    <div class="content-body" style="color: black">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Create Category</h3>
        </div>
        <div class="card">

            <div class="card-body">
                <form
                    action=" @if (isset($category->id)) {{ route('categories.update', $category->id) }} 
                    @else
                    {{ route('categories.store') }} @endif"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    @if (isset($category->id))
                        @method('PUT')
                    @endif

                    @include('components.input_sample', [
                        'name' => 'name',
                        'value' => $category->name ?? '',
                    ])

                    @include('components.input_image', ['value' => $category->image ?? ''])

                    <button type="submit" class="m btn btn-primary">Submit</button>
                </form>
            </div>

            <div class="card-footer clearfix">

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.min.js"
        integrity="sha512-U2WE1ktpMTuRBPoCFDzomoIorbOyUv0sP8B+INA3EzNAhehbzED1rOJg6bCqPf/Tuposxb5ja/MAUnC8THSbLQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- <script type="text/javascript">
            Dropzone.autoDiscover = false;
        $(document).ready(function() {
            $("#imageUpload").Dropzone({
                Dropzone.autoDiscover = true;
                maxFilesize: 1,
                acceptedFiles: '.jpeg,.jpg,.png,.gif'
                maxFilesize: 2,
                uploadMultiple: false,
                addRemoveLinks: true,
                maxFiles: 1,
                autoProcessQueue: false

            })
        })
    </script> --}}
@endsection
