<div class="mb-3"  id="{{$id_div_image ?? 'imageUpload'}}" style="color: black">
    <div class="mb-4" style="color: black;">
        <label for="exampleInputEmail1">{{$imagename ?? 'image'}}</label>
        <input type="file" class="form-control" name="{{ $name ?? 'image_link' }}" value="{{$image_small ?? 'uploads'}}/{{$value}}">

        @if($value)
            
        <img style="height: 100px;width: 120px;" src="/{{$image_small ?? 'uploads'}}/{{$value}}"
            alt="">
        @endif
        @error('image_link')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>


</div>
