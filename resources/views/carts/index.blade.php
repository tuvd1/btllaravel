@extends('layouts.view_customer')
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
@endsection
@section('content')
    <section class="cart">
        <div class="container">
            <div class="cart-top-wrap">
                <div class="cart-top">
                    <div class="cart-top-cart cart-top-item">
                        <i class="fa fa-shopping-bag "></i>
                    </div>
                    <div class="cart-top-address cart-top-item">
                        <i class="fa fa-map-marked-alt "></i>
                    </div>
                </div>
            </div>

        </div>
        <div class="container">
            <div class="cart-content row">
                <div class="cart-content-left">
                    <table>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                            <th>Xóa</th>
                        </tr>
                        @php
                            $total = 0;
                            $suptotal = 0;
                        @endphp
                        @foreach ($carts as $cart)
                            @if (isset($cart->product->image) && $cart->user_id == auth()->user()->id)
                                @php
                                    $suptotal = $cart->product_price * $cart->soluongsp;
                                    $total += $suptotal;
                                    
                                @endphp
                                <tr>
                                    <td><img src="/uploads/{{ $cart->product->image }}" alt=""></td>
                                    <td>{{ $cart->product_name }} </td>
                                    <td>{{ $cart->soluongsp }}</td>
                                    <td>
                                        <p> {{ number_format($suptotal) }}<sup>đ</sup></p>
                                    </td>
                                    <td>
                                        <form action="{{ route('cart.delete', $cart->id) }}">

                                        </form>
                                        <button data-url="{{ route('cart.delete', $cart->id) }}" id="deleteCart"
                                            class="btn btn-danger">Xóa</button>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
                <div class="cart-content-right">
                    <table>
                        <tr>
                            <th colspan="2">Tổng tiền Gio hàng</th>
                        </tr>
                        <tr>
                            <td>Tổng sản phẩm</td>
                            <td>{{ $carts->count() }}</td>
                        </tr>
                        <tr>
                            <td>Tổng tiền hàng</td>
                            <td>
                                <p>{{ number_format($total) }} <sup>đ</sup></p>
                            </td>
                        </tr    >
                        <tr>
                            <td>Tạm tính</td>
                            <td>
                                <p style="color:black;font-weight:bold ;">{{ number_format($total) }}<sub>đ</sub></p>
                            </td>
                        </tr>
                    </table>
                    <div class="cart-content-right-text">
                        <p>Bạn sẽ được miễn phí ship khi đơn hàng của bạn có tổng giá trị trên 2.000.000 đ</p>
                        <p style="color: red;font-weight: bold;">Mua thêm <span style="font-size: 18px;">131.000đ</span> để
                            được miễn phí SHIP</p>
                    </div>
                    <div class="cart-content-right-button">
                        <a href="{{ route('customer') }}">Tiếp tục mua sắm</a>
                        <a href="{{ route('transactions.index') }}">THANH TOÁN</a>
                    </div>

                </div>
            </div>
        </div>

    </section>
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous">
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $('#deleteCart').on('click', function(e) {
                e.preventDefault();
                // alert(1212);
                var url = $(this).attr('data-url');
                var __this = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this imaginary file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal("Đồ ngốc! Giỏ hàng của bạn đã bị xóa!!", {
                                icon: "success",
                            })
                            $.ajax({
                                type: 'DELETE',
                                url: url,

                            }).done(function(response) {
                                __this.parent().parent().remove();
                            })

                        } else {
                            swal("Giỏ hàng của bạn an toàn <3");
                        }
                    });
            })
        });
    </script>
@endsection
