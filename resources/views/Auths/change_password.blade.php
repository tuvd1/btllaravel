<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>

</head>

<body>
    <div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh ;">
        <form action="{{ route('Change.Password') }}" class="border shadow p-3 rounded" method="post"
            style="width:400px">
            @csrf
            @method('PUT')
            @if ( isset($error) )
                <div class="alert alert-danger">
                    {{$error }}
                </div>
            @endif

            <div class="mb-3">
                <label for="token">Mã khôi phục</label>
                <input type="token" class="form-control" id="token" placeholder="token" name="token">
            </div>
            @error('token')
                <p style="color: red">{{ $message }}</p>
            @enderror
            <div class="mb-3">
                <label for="Password">Password</label>
                <input type="password" class="form-control" id="Password" placeholder="Password" name="password">
            </div>
            @error('password')
                <p style="color: red">{{ $message }}</p>
            @enderror
            <div class="mb-3">
                <label for="confirmpassword">Confirm Password</label>
                <input type="password" class="form-control" id="confirmpassword" placeholder="Confirm password"
                    name="confirmpassword">
            </div>
            @error('confirmpassword')
                <p style="color: red">{{ $message }}</p>
            @enderror
            <div class="mb-3">
                <button type="submit" name="login" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
</body>

</html>
