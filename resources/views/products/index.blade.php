@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Danh sách sản phẩm</h3>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="p-2 bd-highlight">
                    <form data-url="{{ route('product.updateDiscount') }}">
                        <select name="slcdiscount" id="slcdiscount">
                            <option value="">------Chọn-----</option>
                            <option value="10">10%</option>
                            <option value="20">20%</option>
                            <option value="30">30%</option>
                            <option value="40">40%</option>
                            <option value="50">50%</option>
                        </select>
                        <button type="submit" id="apply" class="btn btn-primary">apply</button>
                    </form>
                    <a href="{{ route('products.create') }}" class="btn btn-success">Add</a>
                    <a class="btn btn-secondary" href="{{ route('product.softDelete') }}">Restore</a>
                </div>

                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th><input class="checkall" type="checkbox"></th>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Category</th>
                            <th>Image</th>
                            <th>Create</th>
                            <th style="width:20px;">Edit</th>
                            <th style="width:20px;">Delete</th>
                        </tr>

                        @foreach ($products as $product)
                            <tr>
                                <td><input type="checkbox" class="check_chidren" id="discounts" value="{{ $product->id }}" name="discounts"></td>
                                <td>{{ $i++ }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->discount }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td><img style="height: 50px;width: 50px;" src="/uploads/{{ $product->image }}"></td>
                                <td>{{ $product->create }}</td>
                                <td style="width:20px;"><a class="btn btn-primary"
                                        href="{{ route('products.edit', $product->id) }}">Edit</a></td>
                                <td style="width:20px;">
                                    <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    <tbody>



                </table>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $("#apply").on('click', function(e) {
                e.preventDefault();
                // alert(1312);
                const discounts = [];
                $('input[name^="discounts"]').each(function() {
                    if ($(this).is(":checked")) {
                        discounts.push($(this).val());
                    }
                    console.log(discounts);
                });
                var url = $(this).attr('data-url');
                var data = {
                    _token: '{!! csrf_token() !!}',
                    'slcdiscount': $('#slcdiscount').val(),
                    'discounts': discounts,

                };
                console.log(data);
                $.ajax({
                    type: 'post',
                    url: 'product-discount',
                    data: data,
                }).done(function(response) {
                    swal({
                        title: "Good job!",
                        text: "Click Continue",
                        icon: "success",
                        button: "Oke",
                    });
                    window.location.reload();
                }).fail(function(response) {
                    swal({
                        title: "You have not selected discount or product",
                        text: "Click Continue",
                        icon: "error",
                        button: "Oke",
                    });
                });


            })
        })
        $('.checkall').on('click',function(){
            $(this).parents().find('.check_chidren').prop('checked',$(this).prop('checked'));
        })

    </script>
@endsection
