@extends('layouts.view_home')
@section('css')
@endsection
@section('content')
    <div class="content-body">
        <div class="card-header">
            <h3 class="card-title" style="color: blue">Danh sách sản phẩm</h3>
        </div>
        <div class="card">
            <div class="card-body">
                <table id="example2" class="table" style="color: black">
                    <thead>
                        @php
                            $i = 1;
                        @endphp
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th style="width:20px;">Tình trạng</th>
                            <th>Amount</th>
                            <th>Address</th>
                            <th style="width:20px;">Tổng tiền</th>
                            <th style="width:20px;">Ngày</th>
                        </tr>

                        @foreach ($orders as $order)
                            @if (isset($order->product->user_shop_id) && $order->product->user_shop_id == auth()->user()->id)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->price }}</td>
                                    <td>
                                        @if ($order->status == status_on)
                                            <form action="{{ route('order.update', $order->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')

                                                <button class="btn btn-success" type="submit"> Xác nhận </button>
                                            </form>
                                        @else
                                            {{ 'Đã thanh toán' }}
                                        @endif

                                    </td>
                                    <td>{{ $order->soluong }}</td>
                                    <td>{{ $order->transaction->address }}</td>
                                    <td>{{ $order->transaction->total }}</td>
                                    <td>{{ $order->transaction->created_at }}</td>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    <tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('js') 
@endsection
