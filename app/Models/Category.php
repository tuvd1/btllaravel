<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory,SoftDeletes;
    const PAGINATION=10;
    protected $fillable=[
        'name',
        'image',
    ];
    public function products(){
        return $this->hasMany(Product::class,'category_id','id');
    }
    public function whereCategory($id){
        return $this->where('id',$id);
    }
    public function scopeSearch($query){
        if($key=request()->key){

            $key=request()->key;
            $query=$query->where('name','LIKE','%'.$key.'%');
        }
        return$query;
    }
    public function list()
    {
        $categories = Static::search()->paginate(self::PAGINATION);
        return $categories;
    }
}
