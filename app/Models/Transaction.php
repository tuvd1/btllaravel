<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'status',
        'user_id',
        'user_name',
        'user_phone',
        'address',
        'total',
        'payment',
        'payment_info',
    ];
    public function histories()
    {
        return $this->hasMany(History::class, 'transaction_id', 'id');
    }
    public function status($details)
    {
        
        foreach ($details as $detail) {
            $status = $detail->status;
        }
        if ($status == status_off) { 
                $this->find($detail->transaction_id)->update([
                    'status' => status_off,
                ]);
        }        
    }
}
