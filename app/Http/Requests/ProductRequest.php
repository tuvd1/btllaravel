<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
           
            'name' => 'required|max:255',
            'price' => 'required',
            'discount' => 'required',
            'create' => 'required',
            'image_link' =>
            [
                
                File::types(['png', 'jpg'])
            ],
            'category_id' => 'required|exists:categories,id',
            'small_image.*'=>[
                File::types(['png','jpg']),
            ]
        ];
    }
}
