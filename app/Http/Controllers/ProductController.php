<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    private $product;
    private $category;
    private $image;
    public function __construct(Product $product, Category $category, Image $image)
    {
        $this->image = $image;
        $this->product = $product;
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->whereUserProduct(auth()->user()->id)->with('category')->get();
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->all();
        $user = auth()->user()->id;
        return view('products.createorupdate', compact('categories', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        if ($request->has('image_link')) {
            $file = $request->image_link;
            $ext = $request->image_link->extension();
            $file_name = time() . '-' . 'product.' . $ext;
            $file->move(public_path('uploads'), $file_name);
        }
        $new_product = $this->product->updateOrCreate(
            ['id' => $request->id ?? 'null'],
            [
                'name' => $request->name,
                'price' => $request->price,
                'discount' => $request->discount,
                'create' => $request->create,
                'user_shop_id' => $request->user_shop_id,
                'category_id' => $request->category_id,
                'image' => $file_name
            ]
        );


        if ($request->hasFile('small_images')) {
            $this->image->smallImage($request->small_images, $new_product);
        }

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $smallimage = $this->image->whereImage($product->id)->get();
        $categories = $this->category->all();
        $product = $this->product->find($product->id);
        $user = auth()->user()->id;
        $this->authorize('view', $product);
        return view('products.createorupdate', compact('categories', 'product', 'user', 'smallimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        if ($request->hasfile('image_link')) {
            $file = $request->image_link;
            $ext = $request->image_link->extension();
            $file_name = time() . '-' . 'product.' . $ext;
            $file->move(public_path('uploads'), $file_name);
            unlink(public_path('uploads/' . $product->image));
            $request->merge(['image' => $file_name]);
            $this->product->updateOrCreate(
                ['id' => $product->id],
                [
                    'image' => $request->image,
                    'name' => $request->name,
                    'price' => $request->price,
                    'discount' => $request->discount,
                    'create' => $request->create,
                    'user_shop_id' => $request->user_shop_id,
                    'category_id' => $request->category_id,

                ]
            );
        } else {
            $this->product->updateOrCreate(
                ['id' => $product->id],
                [
                    'name' => $request->name,
                    'price' => $request->price,
                    'discount' => $request->discount,
                    'create' => $request->create,
                    'user_shop_id' => $request->user_shop_id,
                    'category_id' => $request->category_id,
                    'image' => $product->image
                ]
            );
        }


        if ($request->hasfile('small_images')) {

            $smallimages = $this->image->whereImage($product->id)->get();
            $this->image->updateImage($smallimages, $request->small_images, $product);
        }

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->product->find($product->id)->delete();
        return redirect()->route('products.index');
    }
    public function listSoftDelete()
    {

        $products = $this->product->with('category')->onlyTrashed()->get();
        return view('products.list_softdelete')->with('products', $products);
    }
    public function restore($id)
    {

        $product = $this->product->onlyTrashed()->where('id', $id)->first();

        $this->authorize('restore', $product);
        $product->restore();
        return redirect()->route('products.index');
    }
    public function realDelete($id)
    {
        $product = $this->product->onlyTrashed()->where('id', $id)->first();
        $images = $this->image->whereImage($id)->get();

        $this->authorize('forceDelete', $product);
        $this->image->deleteImage($images);
        unlink(public_path('uploads/' . $product->image));
        $this->image->whereImage($id)->delete();
        $product->forceDelete();
        return redirect()->back();
    }
    public function updateDiscount(Request $request){
        // dd($request->all());
        foreach($request->discounts as $discount){
            $this->product->updateOrCreate(
                ['id'=>$discount],
                ['discount'=>$request->slcdiscount]
            );
        }
        // for($i=0;$i<count($request->discounts);$i++){
        //    $this->product->find($request->discounts[$i])->update([
        //         'discount'=>$request->discount,
        //     ]);
        // }
        return response()->json(200);
    }
}
