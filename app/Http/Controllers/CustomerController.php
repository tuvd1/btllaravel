<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private $category;
    private $product;
    private $image;
    public function __construct(Category $category, Product $product, Image $image)
    {
        $this->product = $product;
        $this->category = $category;
        $this->image = $image;
    }
    public function index()
    {
        $categories = $this->category->list();
        $products=$this->product->productOrderBy();
        return view('customers.index',compact('categories','products'));
    }
    public function listProduct($id)
    {   
        $category = $this->category->whereCategory($id)->first();
        $products = $this->product->whereCategoryProduct($id)->Search()->get();
        if($category==null){
           return abort(401);
        }
        return view('customers.list_product', compact('category', 'products'));
    }
    public function listDetailProduct($id, $category_id)
    {
        $small_images = $this->image->whereImage($id)->with('product')->get();
        $detailproduct = $this->product->whereProduct($id)->with('category')->first();
        $products = $this->product->whereCategoryProduct($category_id)->get();
        if($small_images==null||$detailproduct==null){
            return abort(401);
        }
        return view('customers.list_detail_product', compact('detailproduct', 'products', 'small_images'));
    }
}
