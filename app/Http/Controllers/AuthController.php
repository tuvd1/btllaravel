<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Mail\SendMail;
use App\Models\Forgot_password;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    private $user;
    private $forgot_password;
    public function __construct(User $user, Forgot_password $forgot_password)
    {
        $this->user = $user;
        $this->forgot_password = $forgot_password;
    }

    public function login(Request $request)
    {
        $request->flash();
        if (Auth::check()) {
            return redirect()->route('home');
        }

        return view('Auths.login');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
    public function logon(Request $request)
    {
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
        ])) {
            if (auth()->user()->role == role_user) {
                return redirect()->route('customer');
            } else {

                return redirect()->route('home');
            }
        }
        return redirect()->route('login');
    }
    public function forgotPassword()
    {
        return view('Auths.forgot_change_password');
    }
    public function checkMail(Request $request)
    {
        $user = $this->user->where('email', $request->email)->first();
        if ($user) {
            $token = substr(md5(rand(0, 999999)), 0, 8);
            $user->forgotPasswords()->create([
                'token' => $token,
            ]);
            $mail = new SendMail($token);
            Mail::to($request->email)->queue($mail);
        }
        return view('Auths.change_password');
    }

    public function ChangePassword(AuthRequest $request)
    {

        $token = $this->forgot_password->whereForgotPassword($request->token)->first();
        if ($token) {

            $this->user->find($token->user_id)->update([
                'password' => $request->password,
            ]);
            $this->forgot_password->find($token->id)->delete();
            return redirect()->route('login');
        } else {
            return view('Auths.change_password')->with(['error' => 'Mã không đúng']);
        }
    }
}
