<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\History;
use App\Models\Transaction;
use Illuminate\Http\Request;

class CheckOutController extends Controller
{
    private $cart;
    private $transaction;
    private $history;
    public function __construct(Cart $cart, Transaction $transaction, History $history)
    {
        $this->history = $history;
        $this->cart = $cart;
        $this->transaction = $transaction;
    }

    public function payment(Request $request)
    {
        $carts = $this->cart->find($request->cart_id);
        $transaction = $this->transaction->updateOrCreate(
            ['id' => $request->id ?? null],
            [
                'user_name' => $request->name,
                'user_id' => $request->user_id,
                'user_phone' => $request->phone,
                'total' => $request->total,
                'address' => $request->address,
            ]
        );
        $this->history->createHistory($transaction, $carts);
        $this->cart->deleteCart($request->cart_id);

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $vnp_TmnCode = "3R0SY9OA"; //Website ID in VNPAY System
        $vnp_HashSecret = "YNRYYOVQQNVAPXLYLBNRUCELCVSPRLOK"; //Secret key
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = route('return.payment');

        $vnp_TxnRef = time();
        $vnp_OrderInfo = $request->name;
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $request->total * 100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = "";
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        if (isset($vnp_Bill_State) && $vnp_Bill_State != "") {
            $inputData['vnp_Bill_State'] = $vnp_Bill_State;
        }

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) { 
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret); //  
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array(
            'code' => '00', 'message' => 'success', 'data' => $vnp_Url
        );
        if (isset($_POST['redirect'])) {
            header('Location: ' . $vnp_Url);
            die();
        } else {
           
            echo json_encode($returnData);
        }
        
        return redirect()->route('transactions.index')->with('success', 'Thanh toán thành công');
    }
    public function returnPayment()
    {
        return view('transactions.payment');
    }
}
